//
//  Shapes.swift
//  SetGame
//
//  Created by Роман Хоменко on 02.03.2022.
//

import SwiftUI

struct Rhombus: Shape {
    func path(in rect: CGRect) -> Path {
        let centerTop = CGPoint(x: rect.midX, y: rect.minY)
        let leftCorner = CGPoint(x: rect.minX, y: rect.midY)
        let rightCorner = CGPoint(x: rect.maxX, y: rect.midY)
        let centerBottom = CGPoint(x: rect.midX, y: rect.maxY)
        
        var p = Path()
        p.move(to: centerTop)
        p.addLine(to: rightCorner)
        p.addLine(to: centerBottom)
        p.addLine(to: leftCorner)
        p.addLine(to: centerTop)
        
        return p
    }
}

struct EmptyShape: Shape {
    func path(in rect: CGRect) -> Path {
        let p = Path()
        return p
    }
    
    
}
