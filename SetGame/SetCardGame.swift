//
//  SetCardGame.swift
//  SetGame
//
//  Created by Роман Хоменко on 25.02.2022.
//

import SwiftUI

class SetCardGame: ObservableObject {
    typealias Card = SetGame<CardSettings>.Card
    
    private static func createSetGame(with settings: CardSettings) -> SetGame<CardSettings> {
        return SetGame<CardSettings>(numberOfCardsOnScreen: settings.cardsOnTable) { index in
            return settings.allGameCards[index]
        }
    }
    
    @Published private(set) var model: SetGame<CardSettings>! = nil
    @Published private var cardSettings: CardSettings
    
    init() {
        cardSettings = CardSettings()
        cardSettings.createCards()
        model = SetCardGame.createSetGame(with: cardSettings)
    }
    
    var cards: Array<SetGame<CardSettings>.Card> {
        model.cards
    }
    
    var numberOfCards: Int {
        return model.numberOfCardsOnTable
    }
    
    func replaceMatchedCrads() {
        model.replaceMatchedCrads()
    }
    
    // MARK: - Intent(s)
    
    func choose(_ card: SetGame<CardSettings>.Card) {
        model.choose(card)
    }
    
    func newGame() {
        cardSettings.createCards()
        model = SetCardGame.createSetGame(with: cardSettings)
    }
    
    func addNewThreeCards() {
        model.increaseCardsOnTableOnThree(numberOfCardsOnScreen: model.cardsWasOnDesk) { index in
            model.cardsWasOnDesk = index
            return cardSettings.allGameCards[index]
        }
    }
    
    func score() -> Int {
        model.score
    }
}
