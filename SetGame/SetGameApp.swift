//
//  SetGameApp.swift
//  SetGame
//
//  Created by Роман Хоменко on 24.02.2022.
//

import SwiftUI

@main
struct SetGameApp: App {
    let game = SetCardGame()
    
    var body: some Scene {
        WindowGroup {
            ContentView(game: game)
        }
    }
}
