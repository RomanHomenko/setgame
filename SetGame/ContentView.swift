//
//  ContentView.swift
//  SetGame
//
//  Created by Роман Хоменко on 24.02.2022.
//

import SwiftUI

struct ContentView: View {
    @ObservedObject var game: SetCardGame
    
    var body: some View {
        VStack {
            HStack {
                label
                Spacer()
                scoreLabe
                scoreLabelMutating
            }
            .padding(.horizontal)
            AspectVGrid(items: game.cards, aspectRatio: 2/3, content: { card in
                if card.isMatched && !card.isChosen {
//                    Rectangle().opacity(0)
                } else {
                    CardView(card: card)
                        .onTapGesture {
                            game.choose(card)
                        }
                }
            })
            Spacer()
            HStack {
                newGameButton
                Spacer()
                addNewThreeCardsButton
            }
            .padding(.horizontal)
        }
        .padding(.horizontal)
    }
    
    var label: some View {
        Text("Set game").font(.largeTitle)
    }
    
    var scoreLabe: some View {
        Text("Score: ").font(.largeTitle)
    }
    
    var scoreLabelMutating: some View {
        Text("\(game.score())").font(.largeTitle)
    }
    
    var newGameButton: some View {
        VStack {
            Button {
                game.newGame()
            } label: {
                VStack {
                    Text("New Game").font(.largeTitle)
                }
            }
        }
    }
    
    var addNewThreeCardsButton: some View {
        VStack {
            Button {
                game.addNewThreeCards()
            } label: {
                Text("Add 3").font(.largeTitle)
            }
        }.disabled(changeButtonState(for: game))
    }
}

struct CardView: View {
    let card: SetGame<CardSettings>.Card
    
    var body: some View {
        GeometryReader { geometry in
            ZStack {
                let shape = RoundedRectangle(cornerRadius: DrawingConstants.cornerRadius)
                shape.fill().foregroundColor(.white)
                shape.strokeBorder(lineWidth: DrawingConstants.lineWidth)
                    
                ZStack {
                    if card.content.numberOfFigure == 1 {
                        VStack {
                            placeFigure(name: card.content.figure)
                        }
                    } else if card.content.numberOfFigure == 2 {
                        VStack {
                            placeFigure(name: card.content.figure)
                            placeFigure(name: card.content.figure)
                        }
                    } else {
                        VStack {
                            placeFigure(name: card.content.figure)
                            placeFigure(name: card.content.figure)
                            placeFigure(name: card.content.figure)
                        }
                    }
                }
                .padding(10)
                
                if card.isChosen && card.isMatched {
                    shape.opacity(0.25).foregroundColor(.green)
                } else if card.isChosen && card.allThreeMissmatchedNow {
                    shape.opacity(0.35).foregroundColor(.black)
                } else if card.isChosen {
                    shape.opacity(0.25)
                }
            }
            .padding(2)
            .foregroundColor(colorOfCards(currentColor: card.content.color))
        }
    }
    
    
    // chose type of figure
    private func placeFigure(name: String) -> some View {
            switch name {
                case "rhombus":
                    return AnyView(placeRhombus(on: card))
                case "rectangle":
                    return AnyView(placeRectangle(on: card))
                case "circle":
                    return AnyView(placeCircle(on: card))
                default:
                    return AnyView(EmptyShape())
            }
    }
    
    // place one Rhomb on card
    private func placeRhombus(on card: SetGame<CardSettings>.Card) -> some View {
        return Rhombus().stroke(lineWidth: DrawingConstants.lineWidth).background(Rhombus().opacity(cardFill(with: card.content.backgroundOfFigure)))
    }
    
    // place one Rectangle on card
    private func placeRectangle(on card: SetGame<CardSettings>.Card) -> some View {
        return Rectangle().stroke(lineWidth: DrawingConstants.lineWidth).background(Rectangle().opacity(cardFill(with: card.content.backgroundOfFigure)))
    }
    
    // place one Circle on card
    private func placeCircle(on card: SetGame<CardSettings>.Card) -> some View {
        return Circle().stroke(lineWidth: DrawingConstants.lineWidth).background(Circle().opacity(cardFill(with: card.content.backgroundOfFigure)))
    }
    
    private struct DrawingConstants {
        static let cornerRadius: CGFloat = 10
        static let lineWidth: CGFloat = 4
        static let fontScale = 0.65
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        let game = SetCardGame()
        
        ContentView(game: game)
            .preferredColorScheme(.dark)
    }
}

// MARK: - UI - dependent func that changes color of cards

private func changeButtonState(for game: SetCardGame) -> Bool {
    
    if game.model.cardsWasOnDesk >= 81 {
        print(game.model.cardsWasOnDesk)
        return true
    } else if game.numberOfCards < 24 {
        return false
    } else {
        return true
    }
}

private func colorOfCards(currentColor: String) -> Color {
    switch currentColor {
        case "red":
            return Color.red
        case "green":
            return Color.green
        case "blue":
            return Color.blue
        default:
            return Color.red
    }
}

private func cardFill(with opacity: String) -> Double {
    switch opacity {
        case "empty":
            return 0
        case "semitransparent":
            return 0.4
        case "filled":
            return 1
        default:
            return 1
    }
}
