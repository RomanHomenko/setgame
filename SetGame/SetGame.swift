//
//  SetGame.swift
//  SetGame
//
//  Created by Роман Хоменко on 25.02.2022.
//

// MARK: - Task for another day
// [x]show on start screen 12 cards
// [x]show no more than 24 cards during game(after implementing func for add 3 new cards)
// [x]add matching algorithm for three cards
// [x]add put away matching cards
// [x]add func to count scores during game(after required tasks)

// MARK: - Required tasks
// [x]support deselection of 1-2 cards per one attempt
// [x]user should see that 3 cards are matched in a moment
// [x]replace 3 matched cards with others from array of cards(after implementing func for add 3 new cards)
// [x]add button to replace three Matched cards(disable button if the desk has 24 cards)
// [x]add New game button with 12 cards on the desk

// [x]fix button state after use 81 cards

// [x]create own Shape struct
import Foundation

struct SetGame<CardContent> where CardContent: Equatable, CardContent: Settings {
    private(set) var cards: Array<Card>
    
    private var first: Int?
    private var second: Int?
    private var third: Int?
    private var matchingOrMissmatchingArray: [Card]?
    
    var score: Int = 0
    var cardsWasOnDesk: Int = 12
    
    var numberOfCardsOnTable: Int {
        var counter: Int = 0
        
        for index in 0..<cards.count {
            if cards[index].isMatched {
                counter += 1
            }
        }
        return cardsWasOnDesk - counter
    }
    
    mutating func choose(_ card: Card) {
        if let chosenIndex = cards.firstIndex(where: { return $0.id == card.id }), !cards[chosenIndex].isMatched {
            if let potentialIndex = first {
                
                if potentialIndex == chosenIndex {
                    cards[chosenIndex].isChosen.toggle()
                    first = nil
                    second = nil
                    return
                }
                if let secondPotentialIndex = second {
                    
                    matchingOrMissmatchingArray?.append(cards[chosenIndex])
                    
                    if secondPotentialIndex == chosenIndex {
                        cards[chosenIndex].isChosen.toggle()
                        second = nil
                        return
                    }
                    
                    checkForMatching(firstIndex: first!, secondIndex: second!, thirdIndex: chosenIndex)
                    
                    first = nil
                    second = nil
                    third = chosenIndex
                } else {
                    second = chosenIndex
                    matchingOrMissmatchingArray?.append(cards[second!])
                }
                
                
            } else {
                for index in cards.indices {
                    cards[index].isChosen = false
                    cards[index].allThreeMissmatchedNow = false
                    matchingOrMissmatchingArray = []
                }
                first = chosenIndex
                matchingOrMissmatchingArray?.append(cards[first!])
            }
            cards[chosenIndex].isChosen.toggle()
        }
    }
    
    mutating func replaceMatchedCrads() {
        var newArr: [Card] = []
        for index in cards.indices {
            if !cards[index].isMatched {
                newArr.append(cards[index])
            }
        }
    }
    
    mutating func scoreCount(for game: Bool) {
        if game == true {
            score += 3
        } else {
            score -= 3
        }
    }
    
    mutating func increaseCardsOnTableOnThree(numberOfCardsOnScreen: Int, createNewCardContent: (Int) -> CardContent) {
        
        let newThreeIndexes = numberOfCardsOnScreen + 3
        
        for index in numberOfCardsOnScreen..<newThreeIndexes {
            
            if index > 80 {
                return
            } else {
                let content = createNewCardContent(index)
                cards.append(Card(content: content, id: index))
                cardsWasOnDesk = newThreeIndexes
            }
        }
        print(cards.count)
    }
    
    init(numberOfCardsOnScreen: Int, createCardContent: (Int) -> CardContent) {
        cards = []
        
        for index in 0..<numberOfCardsOnScreen {
            let content = createCardContent(index)
            cards.append(Card(content: content, id: index))
        }
    }
    
    struct Card: Identifiable {
        var isChosen: Bool = false
        var isMatched: Bool = false
        var content: CardContent
        
        var allThreeMissmatchedNow: Bool = false
        
        let id: Int
    }
}

struct CardSettings: Settings, Equatable {
    var cardsOnTable: Int = 12
    
    var color: String = ""
    var figure: String = ""
    var numberOfFigure: Int = 0
    var backgroundOfFigure: String = ""
    var id: Int = 0
    
    var allGameCards: [CardSettings] = []
    
    mutating func createCards () {
        var idCounter = 0
        
        let chooseColor: [String] = ["red", "green", "blue"]
        let chooseFigure: [String] = ["rhombus", "rectangle", "circle"]
        let chooseNumberOfFigure: [Int] = [1, 2, 3]
        let chooseBackgroundOfFigure: [String] = ["empty", "semitransparent", "filled"]
        
        for colorStep in 0..<chooseColor.count {
            for figureStep in 0..<chooseFigure.count {
                for numberOfFigureStep in 0..<chooseNumberOfFigure.count {
                    for backgroundOfFigureStep in 0..<chooseBackgroundOfFigure.count {
                        allGameCards.append(CardSettings(color: chooseColor[colorStep], figure: chooseFigure[figureStep], numberOfFigure: chooseNumberOfFigure[numberOfFigureStep], backgroundOfFigure: chooseBackgroundOfFigure[backgroundOfFigureStep], id: idCounter))
                        idCounter += 1
                    }
                }
            }
        }
        allGameCards.shuffle()
    }
}

protocol Settings {
    var color: String {get set}
    var figure: String {get set}
    var numberOfFigure: Int {get set}
    var backgroundOfFigure: String {get set}
    var id: Int {get set}
}

// MARK: - For heavy mutating func for checking three cards

extension SetGame {
    mutating func checkForMatching(firstIndex: Int, secondIndex: Int, thirdIndex: Int) {
        if cards[firstIndex].content.color == cards[secondIndex].content.color && cards[firstIndex].content.color == cards[thirdIndex].content.color {
            cards[firstIndex].isMatched = true
            cards[secondIndex].isMatched = true
            cards[thirdIndex].isMatched = true
            
            scoreCount(for: true)
        } else if cards[firstIndex].content.figure == cards[secondIndex].content.figure && cards[firstIndex].content.figure == cards[thirdIndex].content.figure {
            cards[firstIndex].isMatched = true
            cards[secondIndex].isMatched = true
            cards[thirdIndex].isMatched = true
            
            scoreCount(for: true)
        } else if cards[firstIndex].content.numberOfFigure == cards[secondIndex].content.numberOfFigure && cards[firstIndex].content.numberOfFigure == cards[thirdIndex].content.numberOfFigure {
            cards[firstIndex].isMatched = true
            cards[secondIndex].isMatched = true
            cards[thirdIndex].isMatched = true
            
            scoreCount(for: true)
        } else if cards[firstIndex].content.backgroundOfFigure == cards[secondIndex].content.backgroundOfFigure && cards[firstIndex].content.backgroundOfFigure == cards[thirdIndex].content.backgroundOfFigure {
            cards[firstIndex].isMatched = true
            cards[secondIndex].isMatched = true
            cards[thirdIndex].isMatched = true
            
            scoreCount(for: true)
        } else {
            cards[firstIndex].allThreeMissmatchedNow = true
            cards[secondIndex].allThreeMissmatchedNow = true
            cards[thirdIndex].allThreeMissmatchedNow = true
            
            scoreCount(for: false)
        }
    }
}
